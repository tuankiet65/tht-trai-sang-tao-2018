const int relay_sl = 8;
const int relay_pin[relay_sl] = {3, 4, 5, 6, 7, 8, 9, 10};

void relay_setup() {
  for (int i = 0; i < relay_sl; ++i) {
    pinMode(relay_pin[i], OUTPUT);
    relay_set(i, LOW);
  }
}

void relay_set(int id, int state) {
  if (state==HIGH) {state=LOW;} else {state=HIGH;}
  digitalWrite(relay_pin[id], state);
}

float relay_get_bitmask() {
  byte result = 0;
  for (int i = 0; i < relay_sl; ++i) {
    if (digitalRead(relay_pin[i]) == LOW) {
      result |= (1 << i);
    } 
  }

  return result;
}

void relay_command_callback() {
  Serial.println("Got relay command");
  char *arg;
  arg = inut.next();
  Serial.print("argument 1: ");
  Serial.println(arg);
  int relay_id = arg[0] - '0';

  arg = inut.next();
  Serial.print("argument 2: ");
  Serial.println(arg);
  if (strcmp(arg, "ON") == 0) {
    Serial.print("Set ");
    Serial.print(relay_id);
    Serial.println(" to high");
    relay_set(relay_id, HIGH);
  } else {
    relay_set(relay_id, LOW);
  }
}
