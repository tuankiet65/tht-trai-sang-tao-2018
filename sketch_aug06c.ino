#include <TimeLib.h>
#include <Time.h>

#include "DHT.h"
#include <iNut.h>

const int TEMP_TRIGGER = 0;
const int HUMID_TRIGGER = 1;

const int RELAY5_ACTIVE_HOUR = 2;
const int RELAY5_DEACTIVE_HOUR = 3;

const int RELAY6_ACTIVE_HOUR = 4;
const int RELAY6_DEACTIVE_HOUR = 5;

const int RELAY7_ACTIVE_HOUR = 6;
const int RELAY7_DEACTIVE_HOUR = 7;

const int RELAY8_ACTIVE_HOUR = 8;
const int RELAY8_DEACTIVE_HOUR = 9;

iNut inut;

const int DHTPIN = 2;       //Đọc dữ liệu từ DHT11 ở chân 2 trên mạch Arduino
const int DHTTYPE = DHT11;  //Khai báo loại cảm biến, có 2 loại là DHT11 và DHT22

DHT dht(DHTPIN, DHTTYPE);
 
void setup() {
  Serial.begin(115200);
  dht.begin();         // Khởi động cảm biến
  relay_setup();
  inut.setup(3);
  inut.addCommand("RELAY", relay_command_callback);
  inut.addCommand("SET_TIMESTAMP", set_timestamp_command_callback);
  inut.addCommand("SET", settings_command_callback);
  settings_init();
  
  Serial.println("Started");
}

bool in(int a, int b, int c) {
  return ((b >= a) && (b <= c));
}

void loop() {
  time_save();
  
  float h = dht.readHumidity();    //Đọc độ ẩm
  float t = dht.readTemperature(); //Đọc nhiệt độ
  Serial.print("Temp: ");
  Serial.println(t);
  Serial.print("Humid: ");
  Serial.println(h);

  if (t > settings_get(TEMP_TRIGGER)) {
    relay_set(0, HIGH);
//    relay_set(1, HIGH);
  } else {
    relay_set(0, LOW);
//    relay_set(1, LOW);
  }
  
  if (h > settings_get(HUMID_TRIGGER)) {
    relay_set(2, HIGH);
//    relay_set(3, HIGH);
  } else {
    relay_set(2, LOW);
//    relay_set(3, LOW);
  }

  if (in(settings_get(RELAY5_ACTIVE_HOUR), hour(), settings_get(RELAY5_DEACTIVE_HOUR))) {
    relay_set(4, HIGH);
  } else {
    relay_set(4, LOW);
  }

  if (in(settings_get(RELAY6_ACTIVE_HOUR), hour(), settings_get(RELAY6_DEACTIVE_HOUR))) {
    relay_set(5, HIGH);
  } else {
    relay_set(5, LOW);
  }
  
  if (in(settings_get(RELAY7_ACTIVE_HOUR), hour(), settings_get(RELAY7_DEACTIVE_HOUR))) {
    relay_set(6, HIGH);
  } else {
    relay_set(6, LOW);
  }
  
  if (in(settings_get(RELAY8_ACTIVE_HOUR), hour(), settings_get(RELAY8_DEACTIVE_HOUR))) {
    relay_set(7, HIGH);
  } else {
    relay_set(7, LOW);
  }
  
  inut.setValue(0, t);
  inut.setValue(1, h);
  inut.setValue(2, relay_get_bitmask());

  inut.loop();
  settings_loop();
  time_print();
  delay(1000);
}
