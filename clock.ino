unsigned long elapsed_sec;
void time_save() {
  elapsed_sec = millis();
}

unsigned long time_elapsed() {
  return (unsigned long)(millis()) - elapsed_sec;
}

void set_timestamp_command_callback() {
  Serial.println("Got set_timestamp command");
  char *arg = inut.next();
  setTime(strtol(arg, NULL, 10));
  adjustTime(7 * 60 * 60);
  Serial.print("argument: ");
  Serial.println(arg);
  Serial.print("Set current time to ");
  time_print();
}

void time_print() {
  Serial.print(day());
  Serial.print("/");
  Serial.print(month());
  Serial.print("/");
  Serial.print(year());
  
  Serial.print(" ");

  Serial.print(hour());
  Serial.print(":");
  Serial.print(minute());
  Serial.print(":");
  Serial.println(second());
}
void inc_timestamp(unsigned long sec) {
  Serial.print("Compensating ");
  Serial.print(sec);
  Serial.println(" seconds");
  adjustTime(sec);
}

