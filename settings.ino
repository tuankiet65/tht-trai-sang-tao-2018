#include <EEPROM.h>

const char* var_name[10] = {
  "TEMP_TRIGGER", "HUMID_TRIGGER",
  "RELAY5_ACTIVE_HOUR", "RELAY5_DEACTIVE_HOUR",
  "RELAY6_ACTIVE_HOUR", "RELAY6_DEACTIVE_HOUR",
  "RELAY7_ACTIVE_HOUR", "RELAY7_DEACTIVE_HOUR",
  "RELAY8_ACTIVE_HOUR", "RELAY8_DEACTIVE_HOUR"
};
 

byte settings_value[10];
volatile bool settings_changed;

byte settings_get(int id) {
  if (id < 0 || id > 9) return -1;
  return settings_value[id];
}

void settings_set(int id, byte value) {
  Serial.print("Setting ");
  Serial.print(var_name[id]);
  Serial.print(" to ");
  Serial.println(value);
  
  settings_value[id] = value;
  settings_changed = true;
}

void settings_loop() {
  if (settings_changed) {
    EEPROM.put(0, settings_value);
    settings_changed = false;
  }
}

void settings_command_callback() {
  char *arg;
  Serial.println("Got relay command");
  arg = inut.next();
  int id = strtol(arg, NULL, 10);
  Serial.print("argument 1: ");
  Serial.println(arg);
  arg = inut.next();
  byte value = strtol(arg, NULL, 10);
  Serial.print("argument 2: ");
  Serial.println(arg);
  if (value == 255) {
    Serial.println("null value, retaining old value");
  }
  settings_set(id, value);
}

void settings_init() {
  EEPROM.get(0, settings_value);

  Serial.println("Settings: ");
  for (int i = 0; i < 10; ++i) {
    Serial.print(var_name[i]);
    Serial.print(": ");
    Serial.println(settings_value[i]);
  }
}

